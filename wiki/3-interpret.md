# Interpreter (squid/interpret)

🛈 This page is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈


## Options
To generate the documentation elements, the following options are available:
```
squidInterpret·options = <{

  // MAIN OPTIONS
  !name: <string> « name of the module »,
  ?title: <string> « if you want to display a different title in sidebar than the given name »,
  !absolutePath: <string> « absolute path to the folder of this module (without / at end) »,
  !docDir: <string> « the path to the directory where the documentation is stored, relative to the given absolutePath (e.g. "wiki/"), the end slash is mandatory »,
  ?repository: <string> « repository where this package is available (if this is defined, will automatically make an additional "Code, remarks and issues" page) »,

  // MODULE LOGO
  | logosDirUrl: <string> « url to the directory where logo for this documentation page is stored (the logo should be in this directory under the name "<docConfig.name>.png") »,
  | logoUrl: <string> « url where the logo for this documentation is available »,

  // ADDITIONAL OPTIONS
  ?docCopyright: <see squidDisplay·options.docCopyright>,
  ?docLicense: <see squidDisplay·options.docLicense>,
  ?backgroundWallpaper: <see squidDisplay·options.backgroundWallpaper>,

  // OPTIONS NECESSARY ONLY IF YOU USE "install" TYPE
  ?npmAvailable: <boolean|string>@default=false «
    this option is only used if you include standard "install" pages (see below)
    if true, this means that the package is available in npm,
    if string, package is available in npm under this group name (e.g. "mygroup/moduleName"),
    if not defined or false, will propose to install package from repository
  »,

  // OPTIONS NECESSARY ONLY IF YOU USE "demo" TYPE (IF YOU WANT TO DISPLAY YOUR OWN CUSTOM PAGES AND CODE)
  // the following options are useful only if you create demos that need additional pages
  // if you do so, you will also, need to add the additionalPages returned to your app routing
  ?relativeRequirePath: <string> « the path to your module to require it from your app (to create its page), for example, if your module is called myModule and is installed in node_modules, you can just pass here: "myModule" »,
  ?moduleRelativePath: <string> « where is your module located, for example, if you imported it with npm, you should put: "node_modules/" here »,

}>
```


## Folder structure

To be able to generate a documentation display configuration, the directory passed to squid interpret should contain the following:
- `home.md`: content of the main page of the documentation, where you land when you arrive on this page (should be concise).
- any other file or directory, the following types of files and directories are accepted:
  * `<fileName>.md`: a markdown file to display,
  * `<folderName>/\*.md`: a directory containing markdown files to display,
  * `<fileName>.js` (or `<folderName>` containing an `index.js`), this file should export an object containing the following keys):
    - `title`: the title to display in sidebar,
    - `type`: a type that is supported by squid (see below for available types),
    - any additional key necessary for this type of documentation script to be interpreted by squid (see below for more details).


## Types supported by squid:

### > `"install"`

Will display a simple standard install page for a library.

*Additional keys:*
```js
?script: <boolean>@detault=true « pass false here if you do not want the sentence on how to require the module in a script »,
?style: <string> « if the library has stylesheets that should be imported, use this to specify its path »,
?additional: <string[]> « any additional lines of text you want to add to the text of the install page »,
```

### > `"link"`

Will simply open the given url.

*Additional keys:*
```js
!url: <string> « the url to open »,
```

### > `"api"`

Will fetch a full list of all the methods this module is offering, directly from the module's code, according to the specifications in complementary keys.
If this is used, the module must be installed in the node_modules of your app.

*Additional keys:*
```js
documentation: methodPartial<{
  ?name: <string> «
    name of the method to extract from script file
    don't set any name if you don't have a main method to extract from file, but a list of methods (using methods or methodsAutofill)
  »,
  ?matcher: <string> «
    the name of the method to be matched in the script
    if undefined value set in methodPartial.name will be used as matcher
    if false, doc won't be extracted for this method (useful if you want only submethods of an object)
  »,
  ?display: <{
    ?title: <string|false> «
      method title to be displayed before it's descriptions
      if undefined value set in methodPartial.name will be used as title
      if false, title won't be displayed
    »,
    ?comment: <string> « comment displayed under the method title »,
    ?borderColor: <"primary"|"secondary"|"tertiary"|hexColor> «
      if set, will display a colored border on the left side of this method and submethods
      and will also color title with this color
    »,
  }>,
  ?methodsAutofill: <methodsAutofill.type|{
    !type: <"children"|"all"|"prefix"> «
      "children": method children keys will be used as
    »,
    ?excluded: <string[]> « list of automatically matched methods to ignore »,
    ?prefix: <string> «
      if type is "prefix", the method names prefix to match
      if type is "all", all method names will be prefixed with this, if it's not defined it will default to methodPartial.name
    »,
    ?methodName: <string> «
      if type is "children", the method name of which you want children
      if not defined, will default to methodPartial.name
    »,
    ?deepKey: <string> « this is the methodName without the library name, it will be determined automatically if you don't provide it »,
  }> « autofill methods using the chosen preset mechanism »,
  ?methods: <<string|methodPartial>[]> «
    if the script is too complex, you may input here the list of methods manually
    you may also use the methods list to add more than one system to autofill methods
    if you just put a string and not a methodPartial, the string will be translated to { name: <string> },
  »,
}> « object to describe method and submethods and how to extract their documentation from script files »,
```

### > `"demo"`

Allows to create a custom page displaying a demo of the module.

*Additional keys:*
```js
?demo: <string> «
  the name of the folder where to find script and stylesheets for the main demo page,
  see demos[].name option for more details
  if not defined, will display an index of demo pages available
»,
?demos: <<
  |{ intertitle: <string>, }
  |{
    title: <string> « title in the sidebar »,
    name: <string> «
      name of the folder, the script and the stylesheet to use (relative to this directory)
      then you will need to create the folder containing the page script and stylesheet, it should look like this:
        myDemoPage
          ├── myDemoPage.js
          └── myDemoPage.less
    »,
  }
>[]>
```

Then in that directory, you can create pages for each documentation subpage.
Each page should define in it's main script a window.demoStart function.
This function, when executed will create the content of the demo. It will receive a utilities object that contain some methods to make it easier to create a good demo.

#### Utilities methods:

##### section

Make a clean title + description + demo + demo's code paragraph.

```js
utilities.section({
  title: <string>,
  description: <string>,
  demo: <function($container<yquerjObject>)> «
    this function will create the demo,
    it"s code will be displayed below the demo
  »,
});
```
