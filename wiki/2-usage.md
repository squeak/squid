# Usage

Squid is made of two parts:

- `squid/interpret` should be ran server side and produces the documentation,
- `squid` (or `squid/display`) should be ran client side, to create the display.


## Example usage:

#### Server side:

First import the squid interpreter with `const squidInterpret = require("squid/interpret")`.
Then for example, if you use [electrode](https://squeak.eauchat.org/electrode/), you can add a page in your routes with the following configuration:
```javascript
{
  url: "/wiki/",
  title: "My nice wiki page :)",
  moduleConfig: squidInterpret({
    name: "wiki", // this is your module name, (if you want to display a different title on the page than this name, add a "title" option)
    repository: "https://mygitlab.tld/me/my-module/", // don't forget the trailing /
    absolutePath: "/path/to/this/app/", // absolute path to this module in your system
    docDir: "wiki/", // directory where the documentation/wiki/things you want to display in this page are stored in this module
    logoUrl: "/share/myAppLogo.png", // url to your app logo
  }),
}
```

#### Client side:

Then, in your `/wiki/` page's code, you could just have the following lines (still assuming you're using electrode):
```javascript
var squid = require("squid");
squid(page.moduleConfig);
```
And don't forget to import squid styles in your page's stylesheet: `@import "squid/styles/index";`.


## Note:

If you don't like `squid/display`, you can use only the server side part that produces the documentation to be passed to client side, and then interpret the data resulting from `squid/interpret` yourself to make your own display.

You can also use only `squid/display`, but you'll have to pass your data to it mimicking the structure produced by `squid/interpret`.
