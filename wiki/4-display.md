# Displayer (squid/display)

🛈 This page is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈


To display a page properly, squid display needs to receive the following type of configuration:
```
squidDisplay·options = <{

  // GENERAL
  !moduleName: <string> « the name of the module »,
  ?title: <string> « if you want to display a different name than moduleName, you can set what you want to display here »,
  !version: <string>,
  !license: <string>,
  !description: <string>,
  !abstract: <markdownString> « the short content to display in home page (where you land first) »,
  !logoUrl: <string> « url to the logo to display in side bar and abstract page »,

  // PAGES
  ?additionalPages: <<electrode page config>[]> « if this list is not empty, you should make sure that a page is created by electrode for each object in this list »,
  !pages: <{
    !title: <string>,
    !id: <string>,
    !query: <string> « the query of this page in url »,
    !type: <"abstract"|"api"|"demo"|"link"|"markdown"|"markdownFolder">,
    content: <
      any content that the squid:displayDocumentation[type] method can interpret,
      for example if type is "markdown", this is just a markdown text
      see each squid:displayDocumentation method for details on what they can handle
    >,
    ... other keys specificific to the type, see /wiki/3-api.md for more details
  }[]>,

  // ADDITIONAL
  ?repository: <string> « address to the repository where the code of this module is published »,
  ?docCopyright: <string> « a string containing the name of the copyright holder ("copyleft 🄯" will also be appended on the side of it ;)) »
  ?docLicense: <false|string>@default="GNU Free Documentation License" «
    if this is false, no license will be displayed,
    if this is a string, the displayed license will be this string,
    else the "GNU Free Documentation License" will be advertised
  »,
  ?backgroundWallpaper: <string> « url to a wallpaper image if you want to make background of documentation a bit nicer »,

}>
```
