
Squid is an [electrode](https://squeak.eauchat.org/electrode/) library to help you build easily some flexible documentation for your apps, libraries... you can even use it to make simple markdown-based website pages.

Supports many ways to produce your documentation/pages:
  - simple markdown files,
  - getting API documentation directly from your code,
  - pages generated by any custom javascript code,
  - and more...
