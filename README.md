# squid

A library to easily make documentation and wiki pages that can even fetch their content from code itself.

For the full documentation, installation instructions... check [squid documentation page](https://squeak.eauchat.org/libs/squid/).
