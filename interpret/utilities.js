const _ = require("underscore");
const $$ = require("squeak/node");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {

  getTitleFromMarkdown: function (markdownString) {
    return ($$.match(markdownString, /^\#[^\n]*/) || "").replace(/^\#/, "");;
  },

  getLogoUrl: function (moduleConfig) {
    if (moduleConfig.logoUrl) return moduleConfig.logoUrl
    else if (moduleConfig.logosDirUrl) return moduleConfig.logosDirUrl + moduleConfig.name +".png";
  },

};
