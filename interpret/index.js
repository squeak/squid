const fs = require("fs");
const _ = require("underscore");
const $$ = require("squeak/node");
const utilities = require("./utilities");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DOCUMENTATION CONTENT MAKERS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: list of methods to process documentation to produce their content and title
  TYPE:
    <function({
      docConfig <{
        !name: <string>,
        !absolutePath <string> « absolute path to the folder of this module (without / at end) »
      }>,
      packagePageObject: <see identifyDocumentationType·packagePageObject>,
      specificContent: <any>,
      docFileAbsolutePath: <string> « full path, use this with fs »,
      additionalPages: <electrodePage[]>,
    }){@this=configInterpretted}[]>
*/
const documentationContentMaker = {
  api: require("./contentMaker/api"),
  demo: require("./contentMaker/demo"),
  ignore: require("./contentMaker/ignore"),
  images: require("./contentMaker/images"),
  install: require("./contentMaker/install"),
  link: require("./contentMaker/link"),
  markdown: require("./contentMaker/markdown"),
  markdownFolder: require("./contentMaker/markdownFolder"),
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  IDENTIFY DOCUMENTATION TYPE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: fill type in packagePageObject and return additional elements to make specific documentation for this type
  ARGUMENTS: (
    !packagePageObject <{
      id: <string>,
      ¡type: <"abstract"|"markdown"|"markdownFolder"|"api"|"demo"> « will be filled by this function »,
      ¡title: <any> « will be filled later on by documentationContentMaker method »,
      ¡content: <any> « will be filled later on by documentationContentMaker method »,
    }>,
    !docFileAbsolutePath <string> « the full path to the current file being worked on »,
  )
  RETURN: <any>
*/
function identifyDocumentationType (packagePageObject, docFileAbsolutePath) {

  //
  //                              MARKDOWN FOLDER

  if (docFileAbsolutePath.match(/\.md$/)) {
    packagePageObject.type = "markdown";
    return fs.readFileSync(docFileAbsolutePath, "utf8");
  }

  else {

    //
    //                              SCRIPT

    try {
      let packagePageObjectScript = require(docFileAbsolutePath);
      packagePageObject.type = packagePageObjectScript.type;
      return packagePageObjectScript;
    }

    catch (err1) {

      //
      //                              MARKDOWN FOLDER

      try {
        let packagePageObjectMarkdownFolderHome = fs.readFileSync(docFileAbsolutePath +"/home.md", "utf8");
        packagePageObject.type = "markdownFolder";
        return packagePageObjectMarkdownFolderHome;
      }

      //
      //                              UNKNOWN

      catch (err2) {
        $$.log.detailedError(
          "squid/interpret:identifyDocumentationType",
          "Unknown folder type to build documentation.",
          docFileAbsolutePath,
          "ERRORS: ", err1, err2
        );
      };

    };

  };

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: from a documentation directory, produces a configuration that can be processed by squid display
  ARGUMENTS: ( !docConfig <squidInterpret·options> « see wiki/interpret » )
  RETURN: <squidDisplay·options> « see wiki/display »
*/
module.exports = function (docConfig) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // let packageDocumentation = require(req.query.packageName +"/api");
  let package·json = require(docConfig.absolutePath +"/package.json");
  let filesInPackageDocumentationFolder = fs.readdirSync(docConfig.absolutePath +"/"+ docConfig.docDir);
  let packageLicense = fs.readFileSync(docConfig.absolutePath +"/LICENSE", "utf8") || "";
  let homeMarkdownText = fs.readFileSync(docConfig.absolutePath +"/"+ docConfig.docDir +"home.md", "utf8");

  // make list of pages to show
  let pagesList = _.reject(filesInPackageDocumentationFolder, function (fileName) { return fileName == "home.md" });

  // add code and remarks page to pagesList
  if (docConfig.repository) pagesList.push({
    type: "markdown",
    title: "Code, remarks and issues",
    id: "code-issues",
    query: "code-issues",
    content: [
      "# Code, remarks and issues",
      "",
      "The code of "+ docConfig.name +" is FOSS and accessible in [this repository]("+ docConfig.repository +")",
      "",
      "For remarks, questions, criticism, feel very welcome [to post issues]("+ docConfig.repository +"issues).",
    ].join("\n")
  });

  var additionalPages = [];

  // make subpage object
  return {

    //
    //                              MODULE NAME, VERSION, LICENSE, ABSTRACT

    moduleName: docConfig.name,
    title: docConfig.title, // optional
    version: package·json.version,
    license: $$.match(packageLicense, /^[^\n]*/).replace(/^\s*/, "").replace(/\s*$/, ""),
    description: package·json.description,
    abstract: {
      title: docConfig.title || docConfig.name,
      content: homeMarkdownText,
    },
    logoUrl: utilities.getLogoUrl(docConfig), // optional
    backgroundWallpaper: docConfig.backgroundWallpaper, // optional
    repository: docConfig.repository, // optional
    docCopyright: docConfig.docCopyright, // optional
    docLicense: docConfig.docLicense, // optional

    // additional pages that should be created by electrode for this documentation to function properly
    additionalPages: additionalPages,

    //
    //                              MAKE LIST OF PAGES FOR THIS PACKAGE DOCUMENTATION

    pages: _.map(pagesList, function (fileName) {

      // IF IT'S AN OBJECT THAT WAS PASSED, DIRECTLY RETURN IT
      if (_.isObject(fileName)) return fileName;

      // CREATE packagePageObject
      var packagePageObject = {
        id: fileName.replace(/\.[^\.]*$/, ""),
      };
      packagePageObject.query = packagePageObject.id.replace(/^[\d]*\-/, "");

      // MAKE PATHS FROM FILE NAME
      var docFileAbsolutePath = docConfig.absolutePath +"/"+ docConfig.docDir + fileName;

      // FILL type IN packagePageObject AND RETURN ADDITIONAL ELEMENTS
      var additionalElements = identifyDocumentationType(packagePageObject, docFileAbsolutePath);

      // FILL packagePageObject SPECIFIC ELEMENTS FOR THIS TYPE OF PATH
      if (documentationContentMaker[packagePageObject.type]) documentationContentMaker[packagePageObject.type]({
        docConfig: docConfig,
        packagePageObject: packagePageObject,
        specificContent: additionalElements,
        docFileAbsolutePath: docFileAbsolutePath,
        additionalPages: additionalPages,
      })
      else $$.log.detailedError(
        "squid/interpret",
        "Unrecognized type of configuration in '"+ docConfig.name +"' module: '"+ packagePageObject.type +"'",
        "Full configuration object for this page: ",
        packagePageObject
      );

      // RETURN MODULE PAGE OBJECT
      return packagePageObject;

    }),

    //                              ¬
    //

  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
