const _ = require("underscore");
const $$ = require("squeak/node");
const utilities = require("../utilities");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (options) {

  var docFileContent = options.specificContent;
  options.packagePageObject.content = docFileContent;
  options.packagePageObject.title = utilities.getTitleFromMarkdown(options.packagePageObject.content);

};
