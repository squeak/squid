
/**
  DESCRIPTION: just to say this directory should be ignored by squid
  USAGE:
    <folderName>/
    ├── ...
    └── index.js
        └── {
              !type: "ignore",
            }
*/
module.exports = function (options) {};
