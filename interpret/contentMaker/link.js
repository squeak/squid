const _ = require("underscore");
const $$ = require("squeak/node");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (options) {

  var docFileContent = options.specificContent;
  options.packagePageObject.title = docFileContent.title;
  options.packagePageObject.url = docFileContent.url;

};
