const fs = require("fs");
const _ = require("underscore");
const $$ = require("squeak/node");
const utilities = require("../utilities");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (options) {

  let markdownFolderFiles = fs.readdirSync(options.docFileAbsolutePath +"/");
  markdownFolderFiles = _.reject(markdownFolderFiles, function (subfileName) { return subfileName == "home.md"; })

  var packagePageObjectMarkdownFolderHome = options.specificContent;

  options.packagePageObject.title = utilities.getTitleFromMarkdown(packagePageObjectMarkdownFolderHome);
  options.packagePageObject.content = {
    home: packagePageObjectMarkdownFolderHome,
    pages: _.map(markdownFolderFiles, function (subfileName) {
      var content = fs.readFileSync(options.docFileAbsolutePath +"/"+ subfileName, "utf8");
      return {
        hash: subfileName.replace(/\.md$/, "").replace(/[\d]*-/, ""),
        title: utilities.getTitleFromMarkdown(content),
        content: content,
      };
    }),
  };

};
