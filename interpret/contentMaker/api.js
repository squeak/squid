const fs = require("fs");
const _ = require("underscore");
const $$ = require("squeak/node");
require("squeak/extension/string");
var $$log = $$.logger("squid:interpret.api");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  AUTOGENERATE METHODS LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: get list of methods in the given key of the given library
  ARGUMENTS: (<function({
    ?autofill <methodPartial.methodsAutofill>,
    ?method <methodPartial>,
    !lib <any> « the library to list methods from, if it's not defined and "children" is called, will log an error »,
  }[]>)
  RETURN: <methodFull[]>
*/
var autofillMethodsList = {

  //
  //                              CHILDREN

  children: function (autofill, method, lib) {

    // fill autofill object with default values
    if (!autofill.methodName && method.name) autofill.methodName = method.name;
    if (!autofill.methodName) $$log.error("Could not figure out methodName to use to autofill children methods of '"+ method.name +"'.");
    else if (!autofill.deepKey && autofill.methodName) autofill.deepKey = autofill.methodName.replace(/^[^\.]*\./, "");

    // extract children methods
    if (!lib) return $$log.error("Cannot autogenerate methods list if you don't pass the library")
    else {
      var askedMethod = $$.getValue(lib, autofill.deepKey);
      if (!askedMethod) $$log.error("Could not find method from which to extract children, "+ autofill.name +", ", autofill)
      else return _.chain(askedMethod)
              .keys()
              .map(function (childMethodName) {
                return {
                  name: autofill.methodName +"."+ childMethodName,
                  matcher: childMethodName,
                };
              })
              .value()
      ;
    };

  },

  //
  //                              ALL

  all: function (autofill, method, lib) {
    if (!autofill.prefix && method.name) autofill.prefix = method.name;
    return _.map(method.allDescriptions, function (descriptionObject) {
      return {
        name: (autofill.prefix ? autofill.prefix +"." : "")+ descriptionObject.name,
        matcher: descriptionObject.name,
      };
    });
  },

  //
  //                              MATCH

  match: function (autofill, method, lib) {
    return _.filter(method.allDescriptions, function (descriptionObject) {
      return $$.match(descriptionObject.name, autofill.matcher);
    });
  },

  //
  //                              PREFIX

  prefix: function (autofill, method, lib) {
    return _.filter(method.allDescriptions, function (descriptionObject) {
      return $$.match(descriptionObject.name, new RegExp ("^"+ $$.regexp.escape(autofill.prefix)));
    });
  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET SCRIPT TEXT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: get text of a script
  ARGUMENTS: ( path <string> « relative ot the node_modules directory » )
  RETURN: <string|undefined>
*/
function getScriptText (path) {
  try {
    return fs.readFileSync("./node_modules/"+ path +".js", 'utf8');
  }
  catch (e) {
    try {
      return fs.readFileSync("./node_modules/"+ path +"/index.js", 'utf8');
    }
    catch (ee) {
      $$.log.detailedError(
        "squid/interpret/contentMaker/api:getScriptText",
        "Failed to get script text:",
        e,
        ee
      );
    };
  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET DESCRIPTION PARTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: extract description parts if there are some (to organnize them in an object), or just return the given string
  ARGUMENTS: ( description <string> )
  RETURN: <string|descriptionObject>
  TYPES:
    descriptionObject = {
      [key]: <string> « e.g. DESCRIPTION: "this describes a method" »,
    },
*/
function getDescriptionParts (description) {
  var descriptionMatches = description.match(/^[A-Z]+(?=:)/gm);
  if (descriptionMatches) {
    var descriptionsArray = _.rest(description.split(/^[A-Z]+:\s/gm));
    var cleanDescriptionsArray = _.map(descriptionsArray, $$.string.outdentAuto);
    return _.object(descriptionMatches, cleanDescriptionsArray);
  }
  else return description;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET ALL DESCRIPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: get, clean and organnize method's descriptions from script file
  ARGUMENTS: ( !scriptPath <string> « full path to script file » )
  RETURN: <{
    text: <string> « full text of the description »,
    name: <string> « name of the method »,
    nameLine: <string> « name of the method with start and end of line »,
    description: <getDescriptionParts·return> « method description parts »,
  }[]>
*/
function getAllDescriptions (scriptPath) {

  //
  //                              GET PACKAGE SCRIPT TEXT

  let packageScriptText = getScriptText(scriptPath);
  //
  //                              GET LIST OF ALL DESCRIPTIONS IN SCRIPT (do not match directly the needed description because it fails due to a weird thing in regexp)

  let allScriptDescriptions = packageScriptText.match(/\/\*\*[\s\S]*?\*\/\n.*?(?=:|=)/g);

  //
  //                              RETURN AN ORGANNIZED ARRAY OF THE DESCRIPTIONS

  return _.map(allScriptDescriptions, function (desc) {
    var descAsArray = desc.split("\n");

    // LINE CONTAINING METHOD NAME
    var descMethodNameLine = descAsArray[descAsArray.length-1];

    // PRODUCE CLEANED DESCRIPTION
    // remove commenting before and after
    let cleanedDescription = _.clone(descAsArray);
    cleanedDescription.splice(0, 1);
    cleanedDescription.splice(cleanedDescription.length -2, 2);
    // remake a string
    cleanedDescription = cleanedDescription.join("\n");
    // make description object with proper doc indentation
    let descriptionsParts = getDescriptionParts($$.string.outdentAuto(cleanedDescription));

    // RETURN DESCRIPTION DETAILS
    return {
      text: desc,
      // name: descMethodNameLine.replace(/^\s*/, "").replace(/\s*$/, ""), // this doesn't take off "var " prefix...
      name: $$.match(descMethodNameLine, /[^\s]+\s*$/).replace(/\s*$/, ""), // match the method name without any preceding "var" an other sign, or spaces
      nameLine: descMethodNameLine,
      description: descriptionsParts,
    };
  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FILL DOCUMENTATIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: populate or create documentation object
  ARGUMENTS: (
    !method <string|methodPartial|groupPartial> « if a string is passed as method, will generate the object and set this string to the "name" key »,
    ?parentMethod <methodPartial> « method containing this submethod »,
    !root <{
      lib: <object>,
      moduleName: <string>,
      libraryName: <string>,
    }>,
  )
  RETURN: <methodDisplay>
  TYPES:
    methodPartial = <{
      ?name: <string> «
        name of the method to extract from script file
        don't set any name if you don't have a main method to extract from file, but a list of methods (using methods or methodsAutofill)
      »,
      ?matcher: <string> «
        the name of the method to be matched in the script
        if undefined value set in methodPartial.name will be used as matcher
        if false, doc won't be extracted for this method (useful if you want only submethods of an object)
      »,
      ?display: <{
        ?title: <string|false> «
          method title to be displayed before it's descriptions
          if undefined value set in methodPartial.name will be used as title
          if false, title won't be displayed
        »,
        ?comment: <string> « comment displayed under the method title »,
      }>,
      ?methodsAutofill: <methodsAutofill.type|{
        !type: <"children"|"all"|"prefix"> «
          "children": method children keys will be used as
        »,
        ?excluded: <string[]> « list of automatically matched methods to ignore »,
        ?prefix: <string> «
          if type is "prefix", the method names prefix to match
          if type is "all", all method names will be prefixed with this, if it's not defined it will default to methodPartial.name
        »,
        ?methodName: <string> «
          if type is "children", the method name of which you want children
          if not defined, will default to methodPartial.name
        »,
        ?deepKey: <string> « this is the methodName without the library name, it will be determined automatically if you don't provide it »,
      }> « autofill methods using the chosen preset mechanism »,
      ?methods: <<string|methodPartial>[]> «
        if the script is too complex, you may input here the list of methods manually
        you may also use the methods list to add more than one system to autofill methods
        if you just put a string and not a methodPartial, the string will be translated to { name: <string> },
      »,
    }>,
    methodDisplay = <{
      ?title: <string|false>,
      ?comment: <string>,
      ?doc: <{
        [docSessionTitle]: <string>,
      }>,
      ?methods: <methodDisplay[]>,
      ⚠ if at any level a methodDisplay contains no "title", "comment" or "doc", all it's "methods" will be added to the parent methodDisplay
    }>,
*/
var fillDocumentations = function (method, parentMethod, root) {

  // in case just a string is passed
  if (_.isString(method)) method = { name: method, };

  // autofill defaults
  if (_.isUndefined(method.display)) method.display = {};
  if (!_.isUndefined(method.name)) {
    if (_.isUndefined(method.display.title)) method.display.title = method.name;
    if (_.isUndefined(method.matcher)) method.matcher = method.name;
  };

  // get list of descriptions from parent or script file
  if (!method.path && parentMethod) method.allDescriptions = parentMethod.allDescriptions
  else if (method.path) method.allDescriptions = getAllDescriptions(root.moduleName +"/"+ method.path);

  // autofill methods if asked
  if (method.methodsAutofill) {
    if (_.isString(method.methodsAutofill)) method.methodsAutofill = { type: method.methodsAutofill, };
    if (autofillMethodsList[method.methodsAutofill.type]) {
      method.methods = autofillMethodsList[method.methodsAutofill.type](
        method.methodsAutofill,
        method,
        root.lib
      )
      // reject excluded methods
      if (method.methodsAutofill.excluded) method.methods = _.filter(method.methods, function (descriptionObject) {
        return _.indexOf(method.methodsAutofill.excluded, descriptionObject.name) == -1;
      });
    }
    else $$log.error("Cannot autofill methods, autofill method '"+ method.methodsAutofill.type +"' unknown. Available autofill methods are: '"+ _.keys(autofillMethodsList).join(", ") +"'.");
  };

  // fill documentation to display for this method
  if (method.matcher && !_.isString(method.display.doc)) {
    var descriptionObject = _.findWhere(method.allDescriptions, { name: method.matcher, });
    if (!descriptionObject) {
      $$log.error(
        "No documentation found for method: "+ method.name +" using matcher: "+ method.matcher,
        "All descriptions that could be matched: "+ _.pluck(method.allDescriptions, "name").join(", ")
      );
      method.display.doc = "No documentation available.";
    }
    else method.display.doc = descriptionObject.description;
  };

  // populate submethods
  if (method.methods) method.display.methods = _.map(method.methods, function (subMethod) {
    return fillDocumentations(subMethod, method, root);
  });

  // flatten up any submethod that doesn't have any "title", "comment", or "doc" property (remaking the whol method.display.methods array to keep right ordering of methods)
  var newMethodsDisplayList = [];
  _.each(method.display.methods, function (submethodDisplay) {
    if (!submethodDisplay.title && !submethodDisplay.comment && !submethodDisplay.doc) _.each(submethodDisplay.methods, function (subsubmethodDisplay) {
      newMethodsDisplayList.push(subsubmethodDisplay);
    })
    else newMethodsDisplayList.push(submethodDisplay);
  });
  method.display.methods = newMethodsDisplayList;

  // return method with all necessary info
  return method.display;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: get api documentation from library code itself
  ARGUMENTS: (!options <{
    specificContent: <{
      !title: <string>,
      !moduleName: <object> « imported library code, necessary if you want to use "children" autofilling »,
      ?libraryName: <string> «
        set this if the way you name library when you import it is different than the name of the module containing it
        else it will be autofilled with moduleName
      »,
      !lib <any> « the library to list methods from, necessary if you want to use the "children" methodsAutofill »,
      !documentation: <methodPartial> « documentation extracting settings »,
    }>,
    packagePageObject: <{
      title:       <options.specificContent.title>       « autofilled from options.specificContent.title »,
      moduleName:  <options.specificContent.moduleName>  « autofilled from options.specificContent.moduleName »,
      libraryName: <options.specificContent.libraryName> « autofilled from options.specificContent.libraryName »,
      content:     <methodDisplay>                       « autofilled by this script »,
    }>,
  }>)
  RETURN: <void> « options.packagePageObject is filled during the process »
*/
module.exports = function (options) {
  var packagePageObjectScript = options.specificContent;

  options.packagePageObject.content = fillDocumentations(
    packagePageObjectScript.documentation,
    undefined,
    packagePageObjectScript
  );
  options.packagePageObject.title = packagePageObjectScript.title;
  options.packagePageObject.moduleName = packagePageObjectScript.moduleName;
  options.packagePageObject.libraryName = packagePageObjectScript.libraryName || packagePageObjectScript.moduleName;

};
