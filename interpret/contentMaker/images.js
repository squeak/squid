const fs = require("fs");
const _ = require("underscore");
const $$ = require("squeak/node");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: used to display a page with simply a list of images
  USAGE:
    <folderName>/
    ├── 01-<First image title>.<ext> « any file extension the browser supports is supported »
    ├── 02-<Second image title>.<ext>
    ├── ...
    └── index.js
        └── {
              !type: "images",
              !assetsFolderName: <string> «
                name of the folder containing those assets in squik
                this should be a subfolder of "squik/public/share/assets/"
                the best way to add this folder in squik is to enter in squik and then run:
                  `cd public/share/assets`
                  `ln -rs ../../../node_modules/<libName>/wiki/<folderName>/ <assetsFolderName>`
              »,
              ?title: <string>@default=<folderName> « the title the page will have in menu »,
              ?style: <"scroll"|"swipe">@default="scroll" « if "swipe", will display images in a uify.swiper fashion »,
            }
*/
module.exports = function (options) {


  let imageFilesList = fs.readdirSync(options.docFileAbsolutePath +"/");
  imageFilesList = _.reject(imageFilesList, function (subfileName) { return subfileName == "index.js"; })

  options.packagePageObject.title = options.specificContent.title;
  options.packagePageObject.style = options.specificContent.style;
  options.packagePageObject.content = _.map(imageFilesList, function (subfileName) {
    var fileTitle = subfileName.replace(/^\d*-/, "").replace(/\.[^\.]*$/, "");
    return {
      title: fileTitle,
      src: "/share/assets/"+ options.specificContent.assetsFolderName +"/"+ subfileName,
    };
  });

};
