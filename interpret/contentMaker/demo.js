const _ = require("underscore");
const $$ = require("squeak/node");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (options) {
  var packagePageObject = options.packagePageObject;
  var packagePageObjectScript = options.specificContent;
  var additionalPages = options.additionalPages;
  var docConfig = options.docConfig;

  // make title
  packagePageObject.title = packagePageObjectScript.title;

  // if root demo page is already a demo, not an index
  if (packagePageObjectScript.demo) {
    packagePageObject.demoUrl = "/demo/"+ docConfig.name +"/"+ packagePageObjectScript.demo +"/";
    additionalPages.push({
      url: packagePageObject.demoUrl,
      minify: false,
      customPagesFolderPath: docConfig.moduleRelativePath,
      path: docConfig.relativeRequirePath +"/"+ docConfig.docDir + packagePageObject.id +"/"+ packagePageObjectScript.demo +"/",
    });
  };

  // make list of pages
  packagePageObject.content = [];
  _.each(packagePageObjectScript.demos, function (demoObject) {

    // make additional page url
    if (!demoObject.intertitle) {
      // make url of page
      demoObject.url = "/demo/"+ docConfig.name +"/"+ demoObject.name +"/";
      // add page to additionalPages
      additionalPages.push({
        url: demoObject.url,
        minify: false,
        customPagesFolderPath: docConfig.moduleRelativePath,
        path: docConfig.relativeRequirePath +"/"+ docConfig.docDir + packagePageObject.id +"/"+ demoObject.name +"/",
      });
    };

    // add object to pages
    packagePageObject.content.push(demoObject);

  });

};
