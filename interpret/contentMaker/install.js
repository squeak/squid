const _ = require("underscore");
const $$ = require("squeak/node");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

function makeInstallMarkdown (docConfig, packagePageObjectScript) {
  var moduleName = docConfig.name;

  let defaultOptions = {
    script: true,
  };
  options = $$.defaults(defaultOptions, packagePageObjectScript);

  // figure out package install url
  if (docConfig.npmAvailable) var packageInstallUrl = _.isString(docConfig.npmAvailable) ? docConfig.npmAvailable +"/"+ moduleName : moduleName
  else if (docConfig.repository) var packageInstallUrl = docConfig.repository.replace(/^https\:\/\//, "git+https://git@").replace(/\/$/, "") +".git";
  else throw "Cannot make install page, you should either set npmAvailable or pass a repository.";

  // add installation portion
  let markdownArray = [
    "# Installation",
    "",
    "You can install "+ moduleName +" with npm:",
    "",
    "```bash",
    "npm install "+ packageInstallUrl,
    "```",
    "",
  ];

  // default script require code
  if (options.script) markdownArray.push(
    "## Require the script:",
    "If you use [browserify](https://browserify.org/), require "+ moduleName +" in your pages with:",
    "```js",
    "const "+ moduleName +" = require(\""+ moduleName +"\");",
    "```",
    ""
  );

  // simple style
  if (options.style) markdownArray.push(
    "## Require the stylesheet:",
    "If you use [less](http://lesscss.org/), import "+ moduleName +" stylesheet with:",
    "```js",
    "@import \""+ moduleName +"/"+ options.style +"\"",
    "```",
    "If you don't use less, you will need to [interpret this stylesheet](http://lesscss.org/) to convert it to css.",
    "",
    "### Choose a theme:",
    "You will also need a theme containing basic variables, for the styles to work properly.",
    "If you don't want to make the theme yourself, you can just import some default theme from [stylectrode](https://squeak.eauchat.org/libs/stylectrode/).",
    "For example, to import the default base stylectrode theme:",
    " - add stylectrode to your package: `npm install git+https://git@framagit.org/squeak/stylectrode.git`",
    " - add `@import \"stylectrode/themes/default\"` in your stylesheet",
    ""
  );

  // additional things to add in text
  if (options.additional) _.each(options.additional, function (line) {
    markdownArray.push(line);
  });

  return markdownArray.join("\n");

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (options) {

  var packagePageObjectScript = options.specificContent;
  options.packagePageObject.content = makeInstallMarkdown(options.docConfig, packagePageObjectScript);
  options.packagePageObject.title = "Installation";

};
