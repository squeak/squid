var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
require("uify/extension/swiper");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: display images in a scrolling or swiping fashion
  ARGUMENTS: (
    $container <yquerjObject>,
    moduleConfig,
    pageObject <{
      title: <string>,
      style: <"scroll"|"swipe">,
      content: <{
        title: <string>,
        src: <string>,
      }[]>,
    }>
  )
  RETURN: <void>
*/
module.exports = function ($container, moduleConfig, pageObject, uifyWikiPageObject) {
  var $imagesContainer = $container.div({ class: "documentation-images", });

  //
  //                              SWIPING

  if (pageObject.style === "swipe") {
    $imagesContainer.addClass("swiper_container");
    var swiper = uify.swiper({
      $target: $imagesContainer,
      slides: _.pluck(pageObject.content, "src"),
      slidesAreImageUrls: true,
    });
  }

  //
  //                              SCROLLING

  else _.each(pageObject.content, function (imageObj) {
    var $imgGroup = $imagesContainer.div({ class: "image", });
    $imgGroup.img({ src: imageObj.src, });
    $imgGroup.div({ text: imageObj.title, });
  });

  //                              ¬
  //

};
