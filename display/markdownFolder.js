var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/url");
var markdownify = require("markdownify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function openPage ($container, pageObject, hash) {

  $container.empty();
  var openCustomPage;

  if (!hash) var hash = $$.url.hash.getAsString();
  if (hash) openCustomPage = _.findWhere(pageObject.content.pages, { hash: hash, });

  $container.div({
    class: "documentation-markdown",
    html: markdownify(openCustomPage ? openCustomPage.content : pageObject.content.home),
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function updateSidebarHighlight ($sidebarSubtitles) {

  var hash = $$.url.hash.getAsString();
  $sidebarSubtitles.find("a").removeClass("current");
  $sidebarSubtitles.find('a[href="#'+ hash +'"]').addClass("current");

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function ($container, moduleConfig, pageObject, uifyWikiPageObject) {

  openPage($container, pageObject);

  // make sure to remove hash when main title is clicked
  uifyWikiPageObject.$sidebarTitle.click(function () {
    $$.url.hash.set()
    openPage($container, pageObject);
  });

  _.each(pageObject.content.pages, function (subpage) {
    uifyWikiPageObject.$sidebarSubtitles.a({ text: subpage.title, href: "#"+ subpage.hash, }).click(function () {
      openPage($container, pageObject, subpage.hash);
      updateSidebarHighlight(uifyWikiPageObject.$sidebarSubtitles);
    });
  });

  updateSidebarHighlight(uifyWikiPageObject.$sidebarSubtitles);

};
