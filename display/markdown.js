var _ = require("underscore");
var $$ = require("squeak");
var markdownify = require("markdownify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function ($container, moduleConfig, pageObject, uifyWikiPageObject) {
  $container.div({
    class: "documentation-markdown",
    html: markdownify(pageObject.content),
  });
};
