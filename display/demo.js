var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/url");
require("squeak/extension/string");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UTILITIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var utilities = {

  //
  //                              MAKE A SECTION WITH CODE AND IT'S EXECUTION

  section: function (options) {
    var $app = this;

    // MAKE SECTION
    var $section = $app.div({ class: "section", });

    // TITLE
    $section.div({
      class: "demo-title",
      html: options.title,
    });

    // DESCRIPTION
    if (options.description) $section.div({
      class: "demo-description",
      html: options.description,
    });

    // DEMO
    var $demoSection = $section.div({
      class: "demo-section",
    });
    options.demo($demoSection);

    // CODE
    $section.div({ text: "Code:", });
    // make demo function text without first and last line
    var demoFuncText = options.demo.toString();
    var demoFuncContentCleaned = $$.string.outdentAuto(demoFuncText.replace(/^[^\n]*\n\n*/, "").replace(/\s*\n[^\n]*$/, ""));
    $section.div({
      class: "demo-code",
      html: $$.string.htmlify($$.string.escapeHtml(demoFuncContentCleaned)),
    });

  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  IFRAME STYLES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var iframeStyles = `
  body {
    background: transparent;
    overflow: hidden;
    color: black;
  }

  #electrode-app {
    padding: 2em;
  }

  .demo-title {
    font-size: 2em;
    margin-bottom: 0.4em;
    text-align: center;
    font-weight: bold;
    padding: 0.5em;
  }
  .demo-description {
    font-size: 1.2em;
    background: #DDD;
    padding: 0.5em 1em;
  }
  .demo-section {
    padding: 1em;
    background: grey;
  }
  .demo-code {
    padding: 1em;
    background: black;
    color: white;
    font-family: monospace;
  }
`;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHANGE DEMO PAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function changeDemoPage (moduleConfig, demoUrl) {

  $demoContainer.empty();

  // create loading spinner
  var spinner = uify.spinner({ $container: $demoContainer, });

  // create iframe
  let $frame = $demoContainer.iframe({
    class: "demo-iframe",
    src: demoUrl,
  }).on("load", function () {

    // add styles and remove previous page button
    $frame.contents().find("head").append("<style>"+ iframeStyles +"</style>");
    $frame[0].contentWindow.disablePreviousPageButton();

    // start demo
    var boundUtilities = _.mapObject(utilities, function (func) { return _.bind(func, $frame[0].contentWindow.$app); });
    $frame[0].contentWindow.demoStart(boundUtilities);

    // destroy loading spinner
    spinner.destroy();

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE DEMOS INDEX
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function createDemosIndex (moduleConfig, pageObject, $container) {

  _.each(pageObject.content, function (subpageObject) {

    // show intertitle in sidebar
    if (subpageObject.intertitle) $container.div({
      class: "intertitle",
      html: subpageObject.intertitle,
    })

    // show subtitle in sidebar
    else {
      var $subtitle = $container.a({
        html: subpageObject.title,
        href: "#"+ subpageObject.name,
      }).click(function () {
        changeDemoPage(moduleConfig, subpageObject.url);
      });

    };

  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var $demoContainer;

/**
  DESCRIPTION: create demo pages
  ARGUMENTS: (
    $container <yquerjObject>,
    moduleConfig <>,
    pageObject <{
      id: <string>,
      title: <string>,
      query: <string>,
      type: <"demo">,
      ...
      content: <<
        |{ intertitle: <string>, }
        |{
          lib: <any>,
          title: <string>,
          description: <string>,
          demo: <function($container<yquerjObject>)> «
            this function will create the demo,
            it"s content code will be displayed
          »,
        }
      >[]>,
    }>,
    uifyWikiPageObject <{
      $sidebarSubtitles: <yquerjObject>
    }>,
  )
  RETURN: <void>
*/
module.exports = function ($container, moduleConfig, pageObject, uifyWikiPageObject) {

  // MAKE DEMO CONTAINER
  $demoContainer = $container.div({ class: "documentation-demo", });

  // CREATE EACH DEMO MENU IN SIDEBAR
  createDemosIndex(moduleConfig, pageObject, uifyWikiPageObject.$sidebarSubtitles);

  // SEE WHAT TO OPEN NOW
  var hash = $$.url.hash.getAsString();
  if (!hash) {
    // create root demo
    if (pageObject.demoUrl) changeDemoPage(moduleConfig, pageObject.demoUrl)
    // create index of demos in page
    else {
      var $indexContainer = $demoContainer.div({ class: "demo-index", })
      $indexContainer.div({
        class: "title",
        text: moduleConfig.moduleName + " demos",
      });
      createDemosIndex(moduleConfig, pageObject, $indexContainer);
    };
  }
  // or open asked demo
  else _.each(pageObject.content, function (subpageObject) {
    if (hash == subpageObject.name) changeDemoPage(moduleConfig, subpageObject.url);
  });

};
