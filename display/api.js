var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var $ = require("yquerj");
var inView = require("in-view");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UTILITIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: count all submethods of the given method recursively
  ARGUMENTS: (
    count <integer>,,
    method <methodDisplay>,
  )
  RETURN: <integer>
*/
function countAllSubmethods (count, methodDisplay) {
  if (methodDisplay.doc) count++;
  if (methodDisplay.methods) return _.reduce(methodDisplay.methods, countAllSubmethods, count)
  else return count;
};

/**
  DESCRIPTION: display a count of the number of submethods
  ARGUMENTS: ( method <methodDisplay> )
  RETURN: <void>
*/
function displaySubmethodsCount (methodDisplay) {
  var message = "";
  if (methodDisplay.methods) {

    // list direct children methods
    var numberOfMethods = _.compact(_.pluck(methodDisplay.methods, "doc")).length || 0;
    // list all submethods recursively
    var totalNumberOfSubmethods = countAllSubmethods(-numberOfMethods, methodDisplay) - (methodDisplay.doc ? 1 : 0);
    if (totalNumberOfSubmethods < 0) totalNumberOfSubmethods = 0;

    // create message text
    if (numberOfMethods) {
      message += numberOfMethods +" methods";
      if (totalNumberOfSubmethods) message += "<br>("+ totalNumberOfSubmethods +" submethods)";
    }
    else if (totalNumberOfSubmethods) message += totalNumberOfSubmethods +" submethods";

  };
  return '<span>'+ message +'</span>';
};

/**
  DESCRIPTION: make libraries names and urls clickable
  ARGUMENTS: ( string )
  RETURN: <string>
*/
function makeSeeClickable (string) {

  // escape < and >
  string = string.replace(/\</g, "&lt;");
  string = string.replace(/\>/g, "&gt;");

  // make libs name clickable
  var libMethodMatcher = $$.regexp.make("/"+ libraryName.replace(/\$/g, "\\$") +"[\\w\\$\\.]+/g");
  string = string.replace(libMethodMatcher, '<a class="clickable" href="#$&">$&</a>');

  // make urls clickable
  string = string.replace($$.regexp.urlMatcher("g"), '<a class="clickable" href="$&">$&</a>');

  return string;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a method's description
  ARGUMENTS: (
    !$container <yquerjObject>,
    !method <methodFull>,
    !pageObject <pageObject>,
    !level <integer>,
  )
  RETURN: <void>
*/
function displayDoc ($method, methodDisplay, pageObject, level, uifyWikiPageObject) {

  // TOGGLING WRAPP STATE
  function wrappToggle () {
    if ($submethodsArrow.hasClass("wrappon")) {
      $method.removeClass("wrapped").find(".wrapped").removeClass("wrapped");
      $method.find(".wrappon").removeClass("wrappon");
    }
    else {
      $method.addClass("wrapped").find(".method").addClass("wrapped");
      $method.find(".submethods-arrow").addClass("wrappon");
    };
  };

  // TITLEBAR
  var $titlebar = $method.div({ class: "titlebar", });
  var $titleAndComment = $titlebar.div({ class: "title_and_comment", });

  // TITLE
  if (methodDisplay.title) var $title = $titleAndComment.div({
    id: methodDisplay.fullName,
    class: "title",
    html: methodDisplay.title,
  }).click(wrappToggle);

  // COMMENT
  if (methodDisplay.comment) $titleAndComment.div({
    class: "comment",
    html: $$.string.htmlify(methodDisplay.comment),
  });

  // TOGGLE DISPLAY ARROWS
  if (methodDisplay.doc || (methodDisplay.methods && methodDisplay.methods.length)) var $submethodsArrow = $titlebar.div({
    class: "submethods-arrow",
    html: displaySubmethodsCount(methodDisplay),
  }).click(wrappToggle);

  // DOCUMENTATION
  if (methodDisplay.doc) {
    var $methodDescription = $method.div({ class: "desc", });

    // simple string as doc
    if (_.isString(methodDisplay.doc)) {
      var $methodComment = $methodDescription.span({
        class: "desc-comment",
        html: makeSeeClickable(methodDisplay.doc),
      });
      if (methodDisplay.doc == "No documentation available.") $methodDescription.addClass("missing");
    }

    // detailed object doc
    else _.each(methodDisplay.doc, function (val, key) {
      var $elem = $methodDescription.p({ class: "key-"+ key.toLowerCase(), });
      $elem.span({
        class: "desc-title",
        text: key,
      });
      var isMultiline = _.compact(val.split(/\n/)).length > 1;
      $elem[isMultiline ? "div" : "span"]({
        class: "desc-text",
        html: makeSeeClickable(val),
      });
    });

  };

  // ADD TO SIDEBAR
  if (methodDisplay.sidebarTitle !== false && methodDisplay.title) {
    var $additionalLinkInSidebar = uifyWikiPageObject.$sidebarSubtitles.a({
      text: methodDisplay.sidebarTitle || methodDisplay.title,
      href: "#"+ methodDisplay.title,
      class: "sidebar-subtitle sidebar-subtitle-level_"+ level,
    }).click(function () {
      setTimeout(function () {
        $(".squid-body")[0].scrollTop = $(".squid-body")[0].scrollTop - 50;
      }, 100);
    });
    if (methodDisplay.borderColor) {
      if (_.indexOf(["primary", "secondary", "tertiary"], methodDisplay.borderColor) !== -1) $additionalLinkInSidebar.addClass("colored_"+ methodDisplay.borderColor)
      else $additionalLinkInSidebar.css("text-decoration", "underline "+ methodDisplay.borderColor +" 3px");
    };
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY DOCUMENTATION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display documentations recursively
  ARGUMENTS: (
    !$container <yquerjObject>,
    !method <methodFull|groupFull>,
    !pageObject <pageObject>,
    ?level <integer> « level of deepness for submethods »,
  )
  RETURN: void
*/
function displayDocumentation ($container, methodDisplay, pageObject, level, uifyWikiPageObject) {

  // LEVEL
  if (!level) level = 1;

  // CREATE METHOD DISPLAY CONTAINER
  var $method = $container.div({
    class: "method",
    id: methodDisplay.title,
    doc_level: level,
    doc_title: methodDisplay.title,
  });

  // DISPLAY TITLE, COMMENT AND DOC FOR THIS METHOD
  displayDoc($method, methodDisplay, pageObject, level, uifyWikiPageObject);

  // GROUP BORDER
  if (methodDisplay.borderColor) {
    $method.addClass("bordered");
    if (_.indexOf(["primary", "secondary", "tertiary"], methodDisplay.borderColor) !== -1) $method.addClass("bordered_"+ methodDisplay.borderColor)
    else {
      $method.css("border-left-color", methodDisplay.borderColor);
      $method.children(".titlebar").find(".title").css("color", methodDisplay.borderColor);
    };
  };

  // POPULATE SUBMETHODS
  if (methodDisplay.methods && methodDisplay.methods.length) {

    // MAKE SUBMETHODS CONTAINER
    var $subMethods = $method.div({ class: "submethods submethods-"+ level, })

    // DISPLAY SUBMETHODS
    _.each(methodDisplay.methods, function (subMethodDisplay) {
      displayDocumentation($subMethods, subMethodDisplay, pageObject, level + 1, uifyWikiPageObject);
    });

  };

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CURRENT METHOD BOX
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var allMethodsNames;
var $currentMethodBox;
var firstTime = true;
var updateCurrentlyInViewText = _.throttle(function () {

  // ignore scroll function if not in api page
  if (!$(".documentation-api").length) return;

  // get list of methods in view
  var methodsCurrentlyInView = inView('.method').check().current;

  // sort viewed method by their level (minus their order in the list of methods)
  var elem = _.sortBy(methodsCurrentlyInView, function (el) {
    var $el = $(el);
    var methodLevel = $el.attr("doc_level");
    var methodOrderInFullList = _.indexOf(allMethodsNames, $el.attr("doc_title"));
    // index by method level plus by order of method in global list of methods
    return methodLevel - (methodOrderInFullList/allMethodsNames.length);
  }).reverse()[0];

  // change text of "currently displayed" info box
  if (elem) $currentMethodBox.text($(elem).attr("doc_title"));

}, 300)

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function ($container, moduleConfig, pageObject, uifyWikiPageObject) {

  // documentation container
  var $documentation = $container.div({ class: "documentation-api", });

  // all documentation
  libraryName = pageObject.libraryName;
  displayDocumentation($documentation, pageObject.content, pageObject, 0, uifyWikiPageObject);

  // add link to squyntax
  var $mainTitle = $documentation.div({
    class: "link-to-squyntax",
    html: '🛈 This API is written using squyntax notation, information/documentation about it is available <a href="https://squeak.eauchat.org/libs/squyntax/">here</a>. 🛈',
  }, "prepend");

  // add box with currently seen method
  $currentMethodBox = $container.div({ class: "current_method_box", });
  allMethodsNames = _.map($(".method"), function (elem) { return $(elem).attr("doc_title"); });

  // INITIALIZE SCROLL EVENTS ON BODY
  // on scroll change text of "currently displayed" info box
  if (firstTime) {
    $(".uify-wiki-body").scroll(updateCurrentlyInViewText);
    firstTime = false
  };

};
