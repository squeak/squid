var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
require("squeak/extension/url");
var uify = require("uify");
require("uify/extension/wiki");
var displayDocumentation = {
  api: require("./api"),
  demo: require("./demo"),
  install: require("./markdown"),
  images: require("./images"),
  link: require("./link"),
  markdown: require("./markdown"),
  markdownFolder: require("./markdownFolder"),
};

module.exports = function (moduleConfigObject) {

  //
  //                              MAKE PAGES LIST

  var pagesToDisplay = _.filter(moduleConfigObject.pages, function (squidInterpretedPageObject) {
    return squidInterpretedPageObject.type !== "ignore";
  });
  var pages = _.map(pagesToDisplay, function (squidInterpretedPageObject) {
    if (!displayDocumentation[squidInterpretedPageObject.type]) $$.alert.detailedError(
      "squid/display:switchPage",
      "Unknown type of page!",
      squidInterpretedPageObject.type,
      squidInterpretedPageObject
    )
    else return {
      id: squidInterpretedPageObject.query || squidInterpretedPageObject.id,
      title: false,
      sidebarTitle: squidInterpretedPageObject.title,
      content: function ($container, uifyWikiPageObject, uifyWiki) {
        displayDocumentation[squidInterpretedPageObject.type]($container, moduleConfigObject, squidInterpretedPageObject, uifyWikiPageObject);
      },
    };
  });

  //
  //                              DISPLAY WIKI

  var uifyWiki = uify.wiki({
    $container: $app,
    backgroundWallpaper: moduleConfigObject.backgroundWallpaper,
    logoUrl: moduleConfigObject.logoUrl,
    title: moduleConfigObject.title || moduleConfigObject.moduleName,
    abstract: {
      title: '<a href="'+ moduleConfigObject.repository +'" title="'+ (moduleConfigObject.repository ? 'open module repository' : '') +'">'+ (moduleConfigObject.title || moduleConfigObject.moduleName) +'</a>',
      content: function ($container, uifyWikiPageObject, uifyWiki) {
        displayDocumentation.markdown($container, moduleConfigObject, moduleConfigObject.abstract, uifyWikiPageObject);
      },
      footer: function ($container) {
        var licenseText = [];
        if (moduleConfigObject.docCopyright) licenseText.push("© copyright "+ moduleConfigObject.docCopyright +" — copyleft 🄯");
        if (moduleConfigObject.docLicense !== false) licenseText.push("This documentation is licensed under the "+ (_.isString(moduleConfigObject.docLicense) ? moduleConfigObject.docLicense : "GNU Free Documentation License") +".");
        return licenseText.join("<br>");
      },
    },
    sidebarFooter: "version: "+ moduleConfigObject.version +"<br><br>"+ moduleConfigObject.moduleName +" is licensed under<br>"+ moduleConfigObject.license,
    pages: pages,
  });

  //                              ¬
  //

};
